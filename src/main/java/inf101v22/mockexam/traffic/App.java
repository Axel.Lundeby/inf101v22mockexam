package inf101v22.mockexam.traffic;

import javax.swing.JFrame;

import inf101v22.mockexam.traffic.view.Root;

public class App {
    
    public static void main(String[] args) {
        new App();
    }
    
    public App() {
        JFrame frame = new JFrame("INF101 Traffic");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Root view = new Root(null);
        frame.setContentPane(view);

        frame.pack();
        frame.setVisible(true);
    }
}
